package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/jhes/ditenun-go/pkg/cart"
	"gitlab.com/jhes/ditenun-go/pkg/product"
)

func main() {
	// Load config from .env file
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}

	// Open connection to database
	// NOTE: in this example, i'm not using database
	// In real application, you should open connection to database to
	// query data.
	// db, err := gorm.Open(
	// 	os.Getenv("DB_CONNECTION"),
	// 	fmt.Sprintf("%s://%s:%s@%s:%s/%s?sslmode=disable",
	// 		os.Getenv("DB_CONNECTION"),
	// 		os.Getenv("DB_USERNAME"),
	// 		os.Getenv("DB_PASSWORD"),
	// 		os.Getenv("DB_HOST"),
	// 		os.Getenv("DB_PORT"),
	// 		os.Getenv("DB_DATABASE"),
	// 	),
	// )
	// if err != nil {
	// 	logger.Panic(err)
	// }
	// defer db.Close()

	// Web framework using Echo (https://github.com/labstack/echo)
	// NOTE: Echo used for example only
	// You can use gorilla mux or other web / http framework
	srv := echo.New()

	srv.HideBanner = true
	srv.Debug = true

	srv.Use(middleware.Logger())
	srv.Use(middleware.Recover())

	// NOTE: Adding CORS to enable cross origin resource sharing
	srv.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	// The second parameter should be database instance from line 30
	// Since we are not using database here, i will using an empty object (interface{})
	product.Register(srv, nil)
	cart.Register(srv, nil)

	go func() {
		// Starting the server
		if err := srv.Start(":" + os.Getenv("APP_PORT")); err != nil {
			srv.Logger.Info("shutting down the server")
		}
	}()

	// NOTE: code below is not important,
	// This is for handling graceful shutdown
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := srv.Shutdown(ctx); err != nil {
		srv.Logger.Fatal(err)
	}

	<-ctx.Done()
}
