# Ditenun API
This is an example of Ditenun API

## Run the API
* Clone this repository into your local machine
* Using terminal, change directory to project folder
* Copy `.env.example` to `.env`. You can edit the `.env` file as necesarry.
* Run `go mod download` to download external library
* Run `go run main.go` to run the API

## Endpoints
##### Producs
Currently the only available endpoint is to fetch all products `localhost:8080/products`. The JSON returned should be something like:
`
[
    {
        id: 1,
        name: "Ulos",
        price: 1000
    },
    {
        id: 2,
        name: "Kemeja Gorga Batak",
        price: 2000
    },
    {
        id: 3,
        name: "Laptop Gaming Motif Batak",
        price: 10000
    }
]
`

##### Cart
To retrieve cart `http://localhost:8080/cart` using GET method. The JSON returned should be something like:
`
{
    "item": "Ulos",
    "price": "1000"
}
`

To add item into cart, use `http://localhost:8080/cart` using POST method. With body param:
* item
* price