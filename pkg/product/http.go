package product

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/jhes/ditenun-go/pkg/domain"
)

// EchoHTTPHandler represent HTTP handler for session using Echo
type EchoHTTPHandler struct {
	u domain.ProductUsecase
}

// NewEchoHTTPHandler returns an instance of EchoHTTPHandler
func NewEchoHTTPHandler(e *echo.Echo, u domain.ProductUsecase) {
	h := EchoHTTPHandler{
		u: u,
	}

	e.GET("/products", h.Fetch)
	// e.GET("/products/:id", h.GetByID)
	// e.POST("/products", h.Store)
	// e.PATCH("/products/:id", h.Update)
	// e.DELETE("/products/:id", h.Delete)
	// --
	// NOTE: In this example, i only implement fetch products.
	// In real application, all of endpoint above should be available
}

// Fetch returns all products from database
func (h EchoHTTPHandler) Fetch(c echo.Context) error {
	products, err := h.u.Fetch()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": "sorry, we counter an error. Please try again"})
	}

	return c.JSON(http.StatusOK, products)
}
