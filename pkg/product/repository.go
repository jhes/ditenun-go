package product

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/jhes/ditenun-go/pkg/domain"
)

// mysqlProductRepository is implementation of Repository using MySQL
type mysqlProductRepository struct {
	conn *gorm.DB
}

// NewMysqlProductRepository returns an instance of ProductRepository implementation
// using MySQL
func NewMysqlProductRepository(conn *gorm.DB) domain.ProductRepository {
	return &mysqlProductRepository{
		conn: conn,
	}
}

// Fetch returns products from database
func (r *mysqlProductRepository) Fetch() ([]domain.Product, error) {
	// NOTE: in this code, should be perform query from database
	// Example of code to query from databaase:
	// r.conn.Find(&products, "is_active = 1 AND deleted_at IS NULL")
	// But, for example purposes, i will not using database
	// The products is stored in array

	var products = []domain.Product{
		domain.Product{
			ID:    1,
			Name:  "Ulos",
			Price: 1000,
		},
		domain.Product{
			ID:    2,
			Name:  "Kemeja Gorga Batak",
			Price: 2000,
		},
		domain.Product{
			ID:    3,
			Name:  "Laptop Gaming Motif Batak",
			Price: 10000,
		},
	}

	return products, nil
}
