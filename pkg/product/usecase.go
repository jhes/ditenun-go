package product

import (
	"gitlab.com/jhes/ditenun-go/pkg/domain"
)

// productUsecase is implementation of ProductUsecase
type productUsecase struct {
	r domain.ProductRepository
}

// NewProductUsecase returns an instance of usecase using
// using given r as repository
func NewProductUsecase(r domain.ProductRepository) domain.ProductUsecase {
	return &productUsecase{
		r: r,
	}
}

// Fetch returns products from database
func (u *productUsecase) Fetch() ([]domain.Product, error) {
	return u.r.Fetch()
}
