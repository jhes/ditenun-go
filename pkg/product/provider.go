package product

import (
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
)

// Register registers http handler, usecase and repository of token
func Register(e *echo.Echo, db *gorm.DB) {
	NewEchoHTTPHandler(e,
		NewProductUsecase(
			NewMysqlProductRepository(db),
		),
	)
}
