package domain

import (
	"github.com/pkg/errors"
)

var (
	// ErrNotFound is an error when the requested resource is not found
	ErrNotFound = errors.New("the requested resource is not found")
)
