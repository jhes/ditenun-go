package domain

// Cart represent cart instance
// NOTE: In real application, the Item should be an array of item consist of
// SKU of product, quantity, price per item, and subtotal price
// Item  string  `json:"item"` => Item []Item `json:"items"`
type Cart struct {
	Item  string `json:"item" form:"item"`
	Price string `json:"price" form:"price"`
}

// Item represent of cart item
// NOTE: i'm not using this struct for demo purposes
type Item struct {
	ID            int64   `json:"id"`
	Quantity      int64   `json:"quantity"`
	PricePerItem  float64 `json:"price_per_item"`
	PriceSubtotal float64 `json:"price_subtotal"`
}

// CartRepository represent the cart's repository contract
type CartRepository interface {
	Fetch() (Cart, error)
	Add(item, price string) error
}

// CartUsecase represent the cart's usecases
type CartUsecase interface {
	Fetch() (Cart, error)
	Add(item, price string) error
}
