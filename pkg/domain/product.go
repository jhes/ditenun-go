package domain

// Product represent product instance
type Product struct {
	ID    int64   `json:"id"`
	Name  string  `json:"name"`
	Price float64 `json:"price"`
}

// ProductRepository represent the product's repository contract
type ProductRepository interface {
	Fetch() ([]Product, error)
	// GetByID(id int) (Product, error)
	// Store(p Product) (int, error)
	// Update(id int, p Product) error
	// Delete(id int) error
	// --
	// NOTE: In this example, i declared Fetch() method only.
	// In real application, the GetByID, Store, Update and Delete method
	// should be available in ProductRepository
}

// ProductUsecase represent the product's usecases
type ProductUsecase interface {
	Fetch() ([]Product, error)
	// GetByID(id int) (Product, error)
	// Store(p Product) (int, error)
	// Update(id int, p Product) error
	// Delete(id int) error
	// --
	// NOTE: In this example, i declared Fetch() method only.
	// In real application, the GetByID, Store, Update and Delete method
	// should be available in ProductUsecase
}
