package cart

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/jhes/ditenun-go/pkg/domain"
)

// EchoHTTPHandler represent HTTP handler for session using Echo
type EchoHTTPHandler struct {
	u domain.CartUsecase
}

// NewEchoHTTPHandler returns an instance of EchoHTTPHandler
func NewEchoHTTPHandler(e *echo.Echo, u domain.CartUsecase) {
	h := EchoHTTPHandler{
		u: u,
	}

	e.GET("/cart", h.Fetch)
	e.POST("/cart", h.Add)
}

// Fetch returns item from cart
func (h *EchoHTTPHandler) Fetch(c echo.Context) error {
	cart, err := h.u.Fetch()
	if err != nil {
		return c.JSON(http.StatusInternalServerError, map[string]string{"message": "sorry, we counter an error. Please try again"})
	}

	return c.JSON(http.StatusOK, cart)
}

// Add stores new item into cart
func (h *EchoHTTPHandler) Add(c echo.Context) (err error) {
	cart := new(domain.Cart)
	if err = c.Bind(cart); err != nil {
		return
	}

	h.u.Add(cart.Item, cart.Price)

	return c.JSON(http.StatusOK, cart)
}
