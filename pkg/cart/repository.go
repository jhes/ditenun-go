package cart

import (
	"gitlab.com/jhes/ditenun-go/pkg/domain"
)

// inmemCartRepository is implementation of Repository using in memory
// NOTE: In real life, this repository should be stored into simple database
// such as Redis or Memcache
type inmemCartRepository struct {
	cart domain.Cart
}

// NewInmemCartRepository returns an instance of CartRepository implementation
// using in memory
func NewInmemCartRepository(cart domain.Cart) domain.CartRepository {
	return &inmemCartRepository{
		cart: cart,
	}
}

// Fetch retrieves items from cart
func (r *inmemCartRepository) Fetch() (domain.Cart, error) {
	return r.cart, nil
}

// Add stores new item into cart
func (r *inmemCartRepository) Add(item, price string) error {
	r.cart = domain.Cart{
		Item:  item,
		Price: price,
	}

	return nil
}
