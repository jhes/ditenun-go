package cart

import (
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	"gitlab.com/jhes/ditenun-go/pkg/domain"
)

// Register registers http handler, usecase and repository of token
func Register(e *echo.Echo, db *gorm.DB) {
	NewEchoHTTPHandler(e,
		NewCartUsecase(
			NewInmemCartRepository(domain.Cart{}),
		),
	)
}
