package cart

import (
	"gitlab.com/jhes/ditenun-go/pkg/domain"
)

// cartUsecase is implementation of CartUsecase
type cartUsecase struct {
	r domain.CartRepository
}

// NewCartUsecase returns an instance of usecase using
// using given r as repository
func NewCartUsecase(r domain.CartRepository) domain.CartUsecase {
	return &cartUsecase{
		r: r,
	}
}

// Fetch returns items from cart
func (u *cartUsecase) Fetch() (domain.Cart, error) {
	return u.r.Fetch()
}

// Add stores new item to cart
func (u *cartUsecase) Add(item, price string) error {
	return u.r.Add(item, price)
}
